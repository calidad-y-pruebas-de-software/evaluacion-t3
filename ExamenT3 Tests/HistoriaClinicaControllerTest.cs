﻿using Examen_T3.Controllers;
using Examen_T3.Models;
using Examen_T3.Repositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenT3_Tests
{
    class HistoriaClinicaControllerTest
    {
        [Test]
        public void ReturnViewIndexHistoriaClinica()
        {
            var usuarioMock = new Mock<IHistoriaClinicaRepository>();

            var controller = new HistoriaClinicaController(usuarioMock.Object, null);
            var view = controller.Index();

            Assert.IsInstanceOf<ViewResult>(view);
        }
        [Test]
        public void ReturnViewCreateHistoriaClinica()
        {
            var usuarioMock = new Mock<IHistoriaClinicaRepository>();

            var controller = new HistoriaClinicaController(usuarioMock.Object, null);
            var view = controller.Create();

            Assert.IsInstanceOf<ViewResult>(view);
        }

        [Test]
        public void ReturnViewRazasHistoriaClinica()
        {
            var usuarioMock = new Mock<IHistoriaClinicaRepository>();

            var controller = new HistoriaClinicaController(usuarioMock.Object, null);
            var view = controller.Razas(It.IsAny<int>());

            Assert.IsInstanceOf<ViewResult>(view);
        }
        [Test]
        public void ReturnViewMostrarHistoriaClinica()
        {
            var usuarioMock = new Mock<IHistoriaClinicaRepository>();

            var controller = new HistoriaClinicaController(usuarioMock.Object, null);
            var view = controller.Mostrar(It.IsAny<int>());

            Assert.IsInstanceOf<ViewResult>(view);
        }


    }
}
