﻿using System;
using System.Collections.Generic;
using System.Text;
using Examen_T3.Controllers;
using Examen_T3.Models;
using Examen_T3.Repositories;
using Examen_T3.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace ExamenT3_Tests
{
    class AuthControllerTest
    {
        [Test]
        public void ReturnViewIndexAuth()
        {
            var usuarioMock = new Mock<IUsuarioRepository>();

            var controller = new AuthController(usuarioMock.Object, null);
            var view = controller.Login();

            Assert.IsInstanceOf<ViewResult>(view);
        }
        [Test]
        public void UsuarioLogueadoCorrectamente()
        {
            var mockRepository = new Mock<IUsuarioRepository>();
            mockRepository.Setup(o => o.FindUserLogin("admin", It.IsAny<String>()))
                .Returns(new Usuario { Username = "admin", Password = "admin" });

            var cookie = new Mock<ICookieAuthService>();

            var controller = new AuthController(mockRepository.Object, cookie.Object);

            var view = controller.Login("admin", "admin");

            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
        [Test]
        public void UsuarioLogueadoFallido()
        {
            var mockRepository = new Mock<IUsuarioRepository>();
            mockRepository.Setup(o => o.FindUserLogin("admin", It.IsAny<String>()));

            var cookie = new Mock<ICookieAuthService>();

            var controller = new AuthController(mockRepository.Object, cookie.Object);

            var view = controller.Login("admin", "admin");

            Assert.IsInstanceOf<ViewResult>(view);
        }
        [Test]
        public void LogOutCorrecto()
        {
            var mockRepository = new Mock<IUsuarioRepository>();
            var cookie = new Mock<ICookieAuthService>();
            var controller = new AuthController(mockRepository.Object, cookie.Object);
            var view = controller.Logout();

            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
    }
}
