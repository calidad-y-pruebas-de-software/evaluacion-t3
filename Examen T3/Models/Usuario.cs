﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Models
{
    public class Usuario
    {
        public int Id { set; get; }
        public string Username { set; get; }
        public string Password { set; get; }
    }
}
