﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Models
{
    public class HistoriaClinica
    {
        public int Id { set; get; }
        [Required]
        public DateTime FechaRegistro { set; get; }
        [Required]
        public string Nombre { set; get; }
        [Required]
        public DateTime Nacimiento { set; get; }
        [Required]
        public string Sexo { set; get; }
        [Required]
        public string Especie { set; get; }
        [Required]
        public string Raza { set; get; }
        [Required]
        public int Tamaño { set; get; }
        [Required]
        public string Datos { set; get; }
        [Required]
        public string Dueño { set; get; }
        [Required]
        public string Direccion { set; get; }
        [Required]
        public string Telefono { set; get; }
        [Required]
        public string Email { set; get; }
    }
}
