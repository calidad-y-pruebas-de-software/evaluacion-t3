﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Models
{
    public class Raza
    {
        public int Id { set; get; }
        public string Nombre { set; get; }
        public Especie Especie { set; get; }
        public int IdEspecie { set; get; }
    }
}
