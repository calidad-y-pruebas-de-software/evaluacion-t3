﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Models
{
    public class Especie
    {
        public int Id { set; get; }
        public string Nombre { set; get; }
        public List<Raza> Razas { set; get; }
    }
}
