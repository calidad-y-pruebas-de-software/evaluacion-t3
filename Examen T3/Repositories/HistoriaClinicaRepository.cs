﻿using Examen_T3.DB;
using Examen_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Repositories
{
    public interface IHistoriaClinicaRepository
    {
        public List<Especie> GetEspecies();
        public List<Raza> GetRazas(int IdEspecie);
        public void AddHistoriaClinica(HistoriaClinica hc);
        public List<HistoriaClinica> ListaHistoriasClinicas();
        public HistoriaClinica HistoriaClinicaEspecifico(int Id);
        public List<HistoriaClinica> findHCSimilares(string Dueño);
    }
    public class HistoriaClinicaRepository : IHistoriaClinicaRepository
    {
        private readonly AppPruebaContext context;

        public HistoriaClinicaRepository(AppPruebaContext context)
        {
            this.context = context;
        }
        public List<Especie> GetEspecies()
        {
            return context.Especies.ToList();
        }
        public List<Raza> GetRazas(int IdEspecie)
        {
            return context.Razas
                .Where(o =>  o.IdEspecie == IdEspecie)
                .ToList();
        }
        public void AddHistoriaClinica(HistoriaClinica hc)
        {
            var especies = context.Especies.Where(o => o.Id == int.Parse(hc.Especie)).FirstOrDefault();
            hc.Especie = especies.Nombre;
            context.HistoriaClinicas.Add(hc);
            context.SaveChanges();
        }
        public List<HistoriaClinica> ListaHistoriasClinicas()
        {
            return context.HistoriaClinicas.ToList();
        }
        public HistoriaClinica HistoriaClinicaEspecifico(int Id)
        {
            return context.HistoriaClinicas.Where(o => o.Id == Id).FirstOrDefault();
        }
        public List<HistoriaClinica> findHCSimilares(string Dueño)
        {
            return context.HistoriaClinicas.Where(o => o.Dueño == Dueño).ToList();
        }
    }
}
