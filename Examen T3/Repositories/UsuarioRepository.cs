﻿using Examen_T3.DB;
using Examen_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Repositories
{
    public interface IUsuarioRepository
    {
        public Usuario FindUserLogin(string nombreUsuario, string password);

    }
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly AppPruebaContext context;

        public UsuarioRepository(AppPruebaContext context)
        {
            this.context = context;
        }
        public Usuario FindUserLogin(string Username, string Password)
        {
            return context.Usuarios.FirstOrDefault(o => o.Username == Username & o.Password == Password);
        }

    }
}
