﻿using Examen_T3.DB.Configurations;
using Examen_T3.Models;
using Microsoft.EntityFrameworkCore;


namespace Examen_T3.DB
{
    public class AppPruebaContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<HistoriaClinica> HistoriaClinicas { get; set; }
        public DbSet<Especie> Especies{ get; set; }
        public DbSet<Raza> Razas{ get; set; }
        public AppPruebaContext(DbContextOptions<AppPruebaContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioConfigurations());
            modelBuilder.ApplyConfiguration(new HistoriaClinicaConfigurations());
            modelBuilder.ApplyConfiguration(new EspecieConfigurations());
            modelBuilder.ApplyConfiguration(new RazaConfigurations());

        }
    }


}
