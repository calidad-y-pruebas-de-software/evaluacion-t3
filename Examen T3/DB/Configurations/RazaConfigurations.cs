﻿using Examen_T3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.DB.Configurations
{
    public class RazaConfigurations : IEntityTypeConfiguration<Raza>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Raza> builder)
        {
            builder.ToTable("Raza");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Especie)
                .WithMany(o => o.Razas)
                .HasForeignKey(o => o.IdEspecie);
        }
    }
}
