﻿using Examen_T3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.DB.Configurations
{
    public class HistoriaClinicaConfigurations : IEntityTypeConfiguration<HistoriaClinica>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<HistoriaClinica> builder)
        {
            builder.ToTable("HistoriaClinica");
            builder.HasKey(o => o.Id);

        }
    }
}
