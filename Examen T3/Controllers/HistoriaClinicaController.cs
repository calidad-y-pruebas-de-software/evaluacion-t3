﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen_T3.Models;
using Examen_T3.Repositories;
using Examen_T3.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Examen_T3.Controllers
{
    [Authorize]
    public class HistoriaClinicaController : Controller
    {
        private readonly IHistoriaClinicaRepository repository;
        private readonly ICookieAuthService cookieService;
        public HistoriaClinicaController(IHistoriaClinicaRepository repository, ICookieAuthService cookieService)
        {
            this.repository = repository;
            this.cookieService = cookieService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            var hc = repository.ListaHistoriasClinicas(); 
            return View(hc);
        }
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Especies = repository.GetEspecies();
            return View(new HistoriaClinica());
        }
        [HttpPost]
        public IActionResult Create(HistoriaClinica hc)
        {
            
            if (hc == null)
                ModelState.AddModelError("Nombre", "Datos incorrectos");
            
            //if((repository.findHCSimilares(hc.Dueño)).Count() > 0)
            //    ModelState.AddModelError("Dueño", "No puede haber más de un dueño");
            
            if (ModelState.IsValid)
            {
                repository.AddHistoriaClinica(hc);
                return RedirectToAction("Index");
            }
            return View(hc);
        }
        public IActionResult Razas(int IdEspecie)
        {
            var razas = repository.GetRazas(IdEspecie);
            return View(razas);
        }
        [HttpGet]
        public IActionResult Mostrar(int Id)
        {
            var hc = repository.HistoriaClinicaEspecifico(Id);
            return View(hc);
        }

    }
}
