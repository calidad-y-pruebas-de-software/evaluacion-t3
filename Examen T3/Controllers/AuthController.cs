﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Examen_T3.Repositories;
using Examen_T3.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Examen_T3.Controllers
{

        public class AuthController : Controller
        {
            private readonly IUsuarioRepository repository;
            private readonly ICookieAuthService cookieService;
            public AuthController(IUsuarioRepository repository, ICookieAuthService cookieService)
            {
                this.repository = repository;
                this.cookieService = cookieService;
            }

            [HttpGet]
            public IActionResult Login()
            {
                return View();
            }
            [HttpPost]
            public IActionResult Login(string Username, string Password)
            {

                var usuariodb = repository.FindUserLogin(Username, Password);
                if (usuariodb == null)
                {
                    ModelState.AddModelError("Usuario", "Usuario y/o contraseña incorrectos");
                    return View();
                }
                cookieService.SetHttpContext(HttpContext);
                cookieService.SetSessionContext(usuariodb);
                var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name,usuariodb.Username)
                    };

                var Identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(Identity);

                cookieService.Login(principal);
                return RedirectToAction("Index", "Home");
            }

            [Authorize]
            [HttpGet]
            public IActionResult Logout()
            {
                cookieService.SetHttpContext(HttpContext);
                cookieService.LogOut();
                return RedirectToAction("Index", "Home");
            }
        }
    
}
